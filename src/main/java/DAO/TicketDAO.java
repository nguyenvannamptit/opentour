/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Ticket;
import model.Tour;

/**
 *
 * @author Nguyen Van Nam
 */
public class TicketDAO extends DAO {

    public TicketDAO() {
    }

    public void addTicket(Ticket ticket) {
        try {
            String sql = "INSERT INTO ticket(dayStat,discount,amount,customer_id,tour_id) VALUE(?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now()));
            ps.setFloat(2, ticket.getDiscount());
            ps.setInt(3, ticket.getAmount());
            ps.setInt(4, ticket.getCustomer().getId());
            ps.setInt(5, ticket.getTour().getId());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                ticket.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TicketDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        //  return ticket;
    }

}
