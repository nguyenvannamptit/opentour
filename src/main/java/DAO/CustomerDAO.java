/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Nguyen Van Nam
 */
public class CustomerDAO extends DAO {

    public CustomerDAO() {
        super();
    }

    public void addCustomer(Customer customer) {
        try {
            String sqlAddCustomer = "INSERT INTO customer(id,customer_name, address, phone, email) VALUE(?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sqlAddCustomer, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, customer.getId());
            ps.setString(2, customer.getNameCustomer());
            ps.setString(3, customer.getAddress());
            ps.setString(4, customer.getPhone());
            ps.setString(5, customer.getEmail());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                customer.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        //  return customer;
    }

    public boolean checkCustomer(Customer customer) {
        try {
            String sql = "SELECT * FROM customer WHERE customer_name LIKE ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "%" + customer.getNameCustomer() + "%");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                customer.setAddress(rs.getString("address"));
                customer.setPhone(rs.getString("phone"));
                customer.setEmail(rs.getString("email"));
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<Customer> findAll(Customer customer) {
        List<Customer> listCustomer = new ArrayList<>();
        try {
            String sql = "SELECT * FROM customer WHERE customer_name LIKE ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "%" + customer.getNameCustomer() + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                customer.setId(rs.getInt("id"));
                customer.setNameCustomer(rs.getString("customer_name"));
                customer.setAddress(rs.getString("address"));
                customer.setPhone(rs.getString("phone"));
                customer.setEmail(rs.getString("email"));
                listCustomer.add(customer);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listCustomer;
    }
}
