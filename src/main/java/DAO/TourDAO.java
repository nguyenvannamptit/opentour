/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static DAO.DAO.con;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import model.Customer;
import model.Place;
import model.Tour;

/**
 *
 * @author Nguyen Van Nam
 */
public class TourDAO extends DAO {

    public TourDAO() {
        super();
    }

    public List<Tour> showListTour(Place place) {
        List<Tour> listTour = new ArrayList<>();
        try {
            String sql = "SELECT tour1.id,name,departure,arrive,tour1.price FROM place "
                    + "JOIN place_tour1 ON place.id=place_tour1.place_id "
                    + "JOIN tour1 ON tour1.id=place_tour1.tour2_id WHERE place.name_place LIKE ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "%" + place.getName() + "%");
            ResultSet rs = ps.executeQuery();
            System.out.println(rs);
            while (rs.next()) {
                Tour tour = new Tour();
                tour.setId(rs.getInt("id"));
                tour.setName(rs.getString("name"));
                tour.setDeparture(rs.getDate("departure"));
                tour.setArrive(rs.getDate("arrive"));
                tour.setPrice(rs.getFloat("price"));
                listTour.add(tour);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TourDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTour;
    }
}
