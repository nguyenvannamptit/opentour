/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Bill;

/**
 *
 * @author Nguyen Van Nam
 */
public class BillDAO extends DAO {

    public BillDAO() {
    }

    public void addBill(Bill bill) {
        try {
            String sql = "INSERT INTO bill(paymentDate,paymentType,note,user_id,ticket_id) VALUE(?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now()));
            ps.setString(2, bill.getPaymentType());
            ps.setString(3, bill.getNote());
            ps.setInt(4, bill.getUser().getId());
            ps.setInt(5, bill.getTicket().getId());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                bill.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
