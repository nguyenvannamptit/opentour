/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import java.time.*;

/**
 *
 * @author Nguyen Van Nam
 */
public class Ticket implements Serializable {

    private int id;
    private LocalDate dayStat;

    public LocalDate getDayStat() {
        return dayStat;
    }

    public void setDayStat(LocalDate dayStat) {
        this.dayStat = dayStat;
    }

    private Tour tour;
    private int amount;
    private float discount;
    private Customer customer;

    public Ticket(LocalDate dayStat, Tour tour, int amount, float discount, Customer customer) {
        this.dayStat = dayStat;
        this.tour = tour;
        this.amount = amount;
        this.discount = discount;
        this.customer = customer;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Ticket() {
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

}
