/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Nguyen Van Nam
 */
public class Customer implements Serializable {

    private int id;
    private String nameCustomer;
    private String address;
    private String phone;
    private String email;

    public Customer(String nameCustomer, String address, String phone, String email, int number) {
        this.nameCustomer = nameCustomer;
        this.address = address;
        this.phone = phone;
        this.email = email;

    }

    public Customer(int id, String nameCustomer, String address, String phone, String email, int number) {
        this.id = id;
        this.nameCustomer = nameCustomer;
        this.address = address;
        this.phone = phone;
        this.email = email;

    }

    public Customer(String nameCustomer, String address, String phone, String email) {
        this.nameCustomer = nameCustomer;
        this.address = address;
        this.phone = phone;
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Customer() {
    }

    public int getId() {
        return id;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

}
