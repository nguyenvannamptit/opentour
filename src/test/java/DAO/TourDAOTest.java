/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import model.Place;
import model.Tour;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nguyen Van Nam
 */
public class TourDAOTest {

    private TourDAO tourDAO = new TourDAO();

    public TourDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of showListTour method, of class TourDAO.
     */
    @Test
    public void testShowListTour1() {
        Place place = new Place("Long Biên");
        List<Tour> listTour = new TourDAO().showListTour(place);
        Assert.assertNotNull(listTour);
        assertEquals(2, listTour.size());
    }
    public void testShowListTour2() {
        Place place = new Place("Long Biên");
        List<Tour> listTour = new TourDAO().showListTour(place);
        Assert.assertNotNull(listTour);
        assertEquals(2, listTour.size());
    }


}
