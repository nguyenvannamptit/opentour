/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import junit.framework.Assert;
import model.User;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nguyen Van Nam
 */
public class UserDAOTest {

    private UserDAO userDAO = new UserDAO();

    public UserDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of checkLogin method, of class UserDAO.
     */
    @Test
    public void testCheckLogin() {
        User user = new User("a", "1");
        userDAO.checkLogin(user);
        assertEquals(user.getUsername(), "a");
        assertEquals(user.getPassword(), "1");
    }

    @Test
    public void testCheckLogin2() {
        User user = new User("abc", "2");
        userDAO.checkLogin(user);
        assertNotEquals(user.getUsername(), "a");
        assertNotEquals(user.getPassword(), "1");
    }

}
