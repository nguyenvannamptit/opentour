/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nguyen Van Nam
 */
public class CustomerDAOTest {

    private CustomerDAO customerDAO = new CustomerDAO();

    public CustomerDAOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addCustomer method, of class CustomerDAO.
     *
     * @throws java.sql.SQLException
     */
    @Test
    public void testAddCustomer() throws SQLException {
        try {
            CustomerDAO.con.setAutoCommit(false);
            Customer customer = new Customer("z", "z", "0123456789", "z@gmail.com");
            customerDAO.addCustomer(customer);
            Assert.assertEquals(customer.getNameCustomer(), "z");
            Assert.assertEquals(customer.getAddress(), "z");
            Assert.assertEquals(customer.getPhone(), "0123456789");
            Assert.assertEquals(customer.getEmail(), "z@gmail.com");
        } catch (SQLException ex) {
            customerDAO.con.rollback();
            Logger.getLogger(CustomerDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of findAll method, of class CustomerDAO.
     */
    @Test
    public void testFindAll() {
        String key = "xxxxxxxxxx";
        Customer customer = new Customer();
        customer.setNameCustomer(key);
        List<Customer> listRoom = customerDAO.findAll(customer);
        Assert.assertNotNull(listRoom);
        Assert.assertEquals(0, listRoom.size());
        return;
    }

    @Test
    public void testFindAll2() {
        String key = "a";
        Customer customer = new Customer();
        customer.setNameCustomer(key);
        List<Customer> listRoom = customerDAO.findAll(customer);
        Assert.assertNotNull(listRoom);
        Assert.assertEquals(1, listRoom.size());
        return;
    }

}
